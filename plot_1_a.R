library("ggplot2")
library("cmapR")

paad <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Pancreatic.gct")
kirc <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Kidney.gct")
luad <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Lung.gct")
#cesc <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Cervical.gct")
lusc <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Lung_squamous.gct")
stad <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Stomach.gct")
#hnsc <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/head.gct")
coad <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Colon.gct")
skcm <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Skin.gct")
blca <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Bladder.gct")
esca <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Esophagael.gct")
lihc <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Liver_hepa.gct")
#thca <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Thyroid.gct")
#ov <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Ovarian.gct")
gbm <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Glioblastoma.gct")
prad <- parse.gctx("/home/om/Anik/R_tutorial/for_figures/datasets1/Prostate.gct")


df_to_append <- function(s4){
  prf1 <- data.frame(s4@mat[s4@rid=="PRF1",])
  gzma <- data.frame(s4@mat[s4@rid=="GZMA",])
  #add <- data.frame()
  add <- data.frame(prf1[,1],gzma[,1])
  add[,3] <- sqrt(add[,1]*add[,2])
  print(add)
  #final <- add[,3]
  final <- data.frame(add[,3])
  return(final)
}

all <- c(df_to_append(paad),df_to_append(kirc),df_to_append(luad),df_to_append(lusc),df_to_append(stad),df_to_append(coad),df_to_append(skcm)
                 ,df_to_append(blca),df_to_append(esca),df_to_append(lihc),df_to_append(gbm),df_to_append(prad))

png(file='src/fig_1_a.png')
boxplot(all,xlab = "Different TCGA Tumor types",ylab = "Log-Avg Expression(TPM)",main = "Cycolytic Activity")
dev.off()